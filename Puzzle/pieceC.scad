module PuzzlePieceC()
{
cube([30,10,10]);
cube([10,20,10]);
translate([0,0,10])
    cube([20,10,10]);
}

shave(.5) PuzzlePieceC();

module shave(amount)
{
    translate([0,0,-amount])
    {
        difference()
        {
            cube([99,99,99],center=true);
            minkowski()
            {
                difference()
                {
                    cube([100,100,100],center=true);
                    children();
                }
                cube([amount*2,amount*2,amount*2],center=true);
            }
        }
    }
}
    
